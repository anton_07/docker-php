FROM php:7.4-alpine

RUN apk add --no-cache  \
            bash shadow  \
            imagemagick \
            zip \
            imagemagick-dev \
            libjpeg-turbo-dev \
            libmcrypt-dev \
            oniguruma-dev \
            zlib-dev \
            libpng-dev \
            libzip-dev \
    && apk add --no-cache --virtual .phpize-deps ${PHPIZE_DEPS} \
    # install deps
    && docker-php-ext-install exif pdo pdo_mysql mysqli pcntl mbstring zip \
    && docker-php-ext-configure gd --with-jpeg \
    && docker-php-ext-install gd \
    \
    && pecl install imagick && docker-php-ext-enable imagick \
    && pecl install redis && docker-php-ext-enable redis \
    # clear
    && rm -r /tmp/pear \
    runDeps="$( \
    		scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
    			| tr ',' '\n' \
    			| sort -u \
    			| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
    	)"; \
    apk add --no-network --virtual .phpexts-rundeps $runDeps \
    && docker-php-source delete \
    && apk del .phpize-deps

RUN set -eux; \
	docker-php-ext-enable opcache; \
    { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=2'; \
		echo 'opcache.fast_shutdown=1'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini

RUN apk --no-cache add libgomp

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer